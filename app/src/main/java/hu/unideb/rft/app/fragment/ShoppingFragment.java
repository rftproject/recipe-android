package hu.unideb.rft.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hu.unideb.rft.R;
import hu.unideb.rft.adapter.ShoppingAdapter;
import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.utils.SharedPrefUtils;
import hu.unideb.rft.viewmodel.ShoppingViewModel;

public class ShoppingFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private TextView placeholder;
    private ShoppingAdapter adapter;
    private List<Cart> carts = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.shopping_fragment, container, false);
        placeholder = (TextView) view.findViewById(R.id.placeholder);
        setToolbar(view);
        setRecyclerView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ShoppingViewModel mViewModel = ViewModelProviders.of(this)
                .get(ShoppingViewModel.class);

        mViewModel.getShoppingCarts(SharedPrefUtils.getLoginedUserId(getActivity())).observe(this, carts -> {
            if (carts == null) {
                placeholder.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
            } else {
                placeholder.setVisibility(View.INVISIBLE);
                mRecyclerView.setVisibility(View.VISIBLE);
                adapter.setCarts(carts);
            }
        });

    }

    private void setToolbar(View view) {
        setHasOptionsMenu(true);
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) view.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(getResources().getString(R.string.shoppingCart));
    }

    private void setRecyclerView(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.listRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new ShoppingAdapter(getActivity(), carts);
        mRecyclerView.setAdapter(adapter);
    }
}
