package hu.unideb.rft.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import hu.unideb.rft.R;
import hu.unideb.rft.app.MainActivity;
import hu.unideb.rft.asynctask.LoginTask;
import hu.unideb.rft.utils.Utils;

public class LoginFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    private ConstraintLayout parentLayout;
    private TextInputLayout usernameWrapper;
    private TextInputLayout passwordWrapper;
    private Button loginButton;
    private Button registrationButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.login_fragment, container, false);
        parentLayout = (ConstraintLayout) view.findViewById(R.id.loginLayout);
        usernameWrapper = (TextInputLayout) view.findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) view.findViewById(R.id.passwordWrapper);
        loginButton = (Button) view.findViewById(R.id.loginButton);
        registrationButton = (Button) view.findViewById(R.id.registrationButton);

        setupEditTextListeners();
        setupButtonClicks();
        return view;
    }

    private void setupButtonClicks() {
        loginButton.setOnClickListener(view -> login());
        registrationButton.setOnClickListener(this::navigateToRegistrationFragment);
    }

    private void setupEditTextListeners() {
        passwordWrapper.getEditText().setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.hideKeyboard(v, getActivity());
                login();
                handled = true;
            }
            return handled;
        });

        passwordWrapper.getEditText().setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Utils.hideKeyboard(v, getActivity());
            } else {
                Utils.showKeyboard(passwordWrapper.getEditText(), getActivity());
            }
        });

        usernameWrapper.getEditText().setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Utils.hideKeyboard(v, getActivity());
            }
        });
    }

    private void login() {
        Utils.enableTouch(parentLayout, false);

        String userName = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        if (userName.length() == 0 || password.length() == 0) {
            errorLogin();
            return;
        }

        Boolean isLoginSuccess;
        try {
            isLoginSuccess = new LoginTask(userName, password).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Login failed", e);
            isLoginSuccess = false;
        }

        if (isLoginSuccess) {
            startActivity(new Intent(getActivity(), MainActivity.class));
        } else {
            errorLogin();
        }
    }

    private void errorLogin() {
        Utils.enableTouch(parentLayout, true);
        Utils.showToast(getActivity(), getResources().getString(R.string.errorLogin), Toast.LENGTH_LONG);
    }

    private void navigateToRegistrationFragment(View v) {
        Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_registrationFragment);
    }
}