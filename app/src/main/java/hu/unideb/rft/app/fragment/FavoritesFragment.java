package hu.unideb.rft.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import hu.unideb.rft.R;
import hu.unideb.rft.adapter.FavoritesAdapter;
import hu.unideb.rft.apicall.model.Favorite;
import hu.unideb.rft.utils.RestApiUtils;
import hu.unideb.rft.utils.SharedPrefUtils;

public class FavoritesFragment extends Fragment {
    private GridView gridView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.favorites_fragment, container, false);
        gridView = (GridView) view.findViewById(R.id.gridview);
        setToolbar(view);

        final Long userId = SharedPrefUtils.getLoginedUserId(getActivity());
        final List<Favorite> favorites = RestApiUtils.getFavoritesByUserId(userId);
        FavoritesAdapter adapter = new FavoritesAdapter(getActivity(), favorites);
        gridView.setAdapter(adapter);

        setGridViewOnClick(favorites);
        return view;
    }

    private void setToolbar(View view) {
        setHasOptionsMenu(true);
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) view.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(getResources().getString(R.string.favorites));
    }

    private void setGridViewOnClick(List<Favorite> favorites) {
        gridView.setOnItemClickListener((parent, v, position, id) -> {
            Favorite selected = favorites.get(position);

            Bundle bundle = new Bundle();
            bundle.putString("uri", selected.getUri());
            Navigation.findNavController(v).navigate(R.id.action_favoritesFragment_to_detailsFragment, bundle);
        });
    }

}
