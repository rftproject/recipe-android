package hu.unideb.rft.app.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import hu.unideb.rft.R;
import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.apicall.model.Favorite;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.utils.MyGlideModule;
import hu.unideb.rft.utils.RestApiUtils;
import hu.unideb.rft.utils.SharedPrefUtils;

public class DetailsFragment extends Fragment {
    private String uri;
    private Recipe recipe;
    private Long userId;
    private ImageView favoriteIcon;
    private ImageView cartIcon;
    private Boolean isFavorite;
    private Boolean isInCart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.details_fragment, container, false);
        if (getArguments() != null) {
            uri = getArguments().getString("uri");
            recipe = RestApiUtils.getRecipeById(uri);
        } else {
            Navigation.findNavController(view).navigate(R.id.action_detailsFragment_to_mainFragment);
        }

        userId = SharedPrefUtils.getLoginedUserId(getActivity());
        favoriteIcon = (ImageView) view.findViewById(R.id.favoriteIcon);
        cartIcon = (ImageView) view.findViewById(R.id.cartIcon);

        setToolbar(view);
        MyGlideModule.loadImage(getActivity(), recipe.getImage(), view.findViewById(R.id.detailsImage));
        setTextViews(view);
        setRadioButtons(view, recipe.getIngredientLines());
        setInitialIconsDrawable();
        setIconsOnClick();
        return view;
    }

    private void setToolbar(View view) {
        setHasOptionsMenu(true);
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) view.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(recipe.getLabel());
    }

    private void setTextViews(View v) {
        TextView yieldTV = (TextView) v.findViewById(R.id.yieldNum);
        TextView caloriesTV = (TextView) v.findViewById(R.id.caloriesNum);

        yieldTV.setText(String.valueOf(recipe.getYield().intValue()));
        caloriesTV.setText(String.valueOf(recipe.getCalories().intValue()));
    }

    private void setRadioButtons(View v, List<String> ingredients) {
        LinearLayout checkboxGroup = (LinearLayout) v.findViewById(R.id.checkboxGroup);
        for (int i = 0; i < ingredients.size(); i++) {
            CheckBox checkBox = new CheckBox(getActivity());
            checkBox.setId(i);
            checkBox.setText(ingredients.get(i));

            LinearLayout.LayoutParams checkParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            checkParams.setMargins(10, 8, 8, 8);
            checkParams.gravity = Gravity.START;
            checkboxGroup.addView(checkBox, checkParams);

        }
    }

    private void setInitialIconsDrawable() {
        isFavorite = RestApiUtils.isFavorite(new Favorite(uri, userId));
        isInCart = RestApiUtils.isInCart(new Cart(uri, userId));
        setFavoriteIcon();
        setCartIcon();
    }

    private void setIconsOnClick() {
        favoriteIcon.setOnClickListener(v -> {
            isFavorite = !isFavorite;
            setFavoriteIcon();

            if (!isFavorite) {
                RestApiUtils.deleteFavoriteByUriAndUserId(new Favorite(recipe.getUri(), userId));
            } else {
                RestApiUtils.addFavorite(new Favorite(
                        recipe.getUri(),
                        userId,
                        recipe.getLabel(),
                        recipe.getIngredientLines(),
                        recipe.getImage()));
            }
        });

        cartIcon.setOnClickListener(v -> {
            isInCart = !isInCart;
            setCartIcon();

            if (!isInCart) {
                RestApiUtils.deleteCartByUriAndUserId(new Cart(recipe.getUri(), userId));
            } else {
                RestApiUtils.addCart(new Cart(
                        recipe.getUri(),
                        userId,
                        recipe.getLabel(),
                        recipe.getIngredientLines(),
                        recipe.getImage()));
            }
        });
    }

    private void setFavoriteIcon() {
        if (isFavorite) {
            favoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.favorite));
        } else {
            favoriteIcon.setImageDrawable(getResources().getDrawable(R.drawable.not_favorite));
        }
    }

    private void setCartIcon() {
        if (isInCart) {
            cartIcon.setImageDrawable(getResources().getDrawable(R.drawable.cart));
        } else {
            cartIcon.setImageDrawable(getResources().getDrawable(R.drawable.not_cart));
        }
    }
}
