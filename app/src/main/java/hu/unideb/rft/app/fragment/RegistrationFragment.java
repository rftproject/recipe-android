package hu.unideb.rft.app.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import hu.unideb.rft.BuildConfig;
import hu.unideb.rft.R;
import hu.unideb.rft.apicall.model.RegistrationRequest;
import hu.unideb.rft.apicall.model.RegistrationResponse;
import hu.unideb.rft.asynctask.RegistrationTask;
import hu.unideb.rft.utils.Utils;
import hu.unideb.rft.utils.ValidationMessages;

import static android.widget.LinearLayout.LayoutParams;

public class RegistrationFragment extends Fragment {
    private ConstraintLayout parentLayout;
    private LinearLayout messagesLayout;
    private TextInputLayout emailWrapper;
    private TextInputLayout nameWrapper;
    private TextInputLayout passwordWrapper;
    private TextInputLayout confirmPasswordWrapper;
    private Button regButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.registration_fragment, container, false);
        parentLayout = (ConstraintLayout) view.findViewById(R.id.registrationLayout);
        messagesLayout = (LinearLayout) view.findViewById(R.id.messagesLayout);
        emailWrapper = (TextInputLayout) view.findViewById(R.id.emailWrapper);
        nameWrapper = (TextInputLayout) view.findViewById(R.id.nameWrapper);
        passwordWrapper = (TextInputLayout) view.findViewById(R.id.passwordWrapper);
        confirmPasswordWrapper = (TextInputLayout) view.findViewById(R.id.confirmPasswordWrapper);
        regButton = (Button) view.findViewById(R.id.registrationButton);

        setupEditTextListener();
        setupRegButtonClick();
        return view;
    }

    private void registration(View view) {
        Utils.enableTouch(parentLayout, false);
        if (messagesLayout.getChildCount() != 0) {
            messagesLayout.removeAllViews();
        }

        final RegistrationRequest request = new RegistrationRequest(
                nameWrapper.getEditText().getText().toString(),
                passwordWrapper.getEditText().getText().toString(),
                confirmPasswordWrapper.getEditText().getText().toString(),
                emailWrapper.getEditText().getText().toString(),
                nameWrapper.getEditText().getText().toString()
        );

        List<ValidationMessages> messages = request.validate();
        if (messages.size() != 0) {
            checkValidationMessages(messages);
            return;
        }

        RegistrationTask task = new RegistrationTask();
        final RegistrationResponse response = task.registration(request);

        if (response == null || response.getErrorCode().equals("200")) {
            Utils.enableTouch(parentLayout, true);
            return;
        }

        showActivateAccountPopup(view);
    }

    private void checkValidationMessages(List<ValidationMessages> messages) {
        for (ValidationMessages msg : ValidationMessages.values()) {
            if (messages.contains(msg)) {
                addRegistrationErrorMessage(msg);
            }
        }
        Utils.enableTouch(parentLayout, true);
    }

    private void addRegistrationErrorMessage(ValidationMessages msg) {
        TextView tv = new TextView(getActivity());
        tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        tv.setPadding(0, 0, 0, 8);
        tv.setTextColor(Color.RED);
        int resId = getResources().getIdentifier(msg.getMessage(), "string", BuildConfig.APPLICATION_ID);
        tv.setText(getResources().getString(resId));
        messagesLayout.addView(tv);
    }

    private void setupEditTextListener() {
        confirmPasswordWrapper.getEditText().setOnEditorActionListener((view, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.hideKeyboard(view, getActivity());
                registration(view);
                handled = true;
            }
            return handled;
        });
    }

    private void setupRegButtonClick() {
        regButton.setOnClickListener(this::registration);
    }

    private void showActivateAccountPopup(View parentView) {
        View layout = getLayoutInflater().inflate(R.layout.popup_layout, null);

        PopupWindow popupWindow = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(parentLayout, Gravity.CENTER, 0, 0);

        Button okButton = (Button) layout.findViewById(R.id.okPopupButton);
        okButton.setOnClickListener(v -> {
            Navigation.findNavController(parentView).navigate(R.id.action_registrationFragment_to_loginFragment);
            Utils.enableTouch(parentLayout, true);
            popupWindow.dismiss();
        });
    }
}
