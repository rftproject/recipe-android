package hu.unideb.rft.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import hu.unideb.rft.R;
import hu.unideb.rft.adapter.MainAdapter;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.app.AuthenticationActivity;
import hu.unideb.rft.utils.SharedPrefUtils;
import hu.unideb.rft.utils.Utils;
import hu.unideb.rft.viewmodel.MainViewModel;

public class MainFragment extends Fragment {
    private MainViewModel mViewModel;
    private RecyclerView mRecyclerView;
    private ImageView placeholder;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout searchLayout;
    private EditText searchEditText;
    private MenuItem searchItem;
    private MainAdapter adapter;
    private List<Recipe> mDataset = new ArrayList<>();
    private String ingredients;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.main_fragment, container, false);
        placeholder = (ImageView) view.findViewById(R.id.placeholder);
        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawerLayout);
        searchLayout = (RelativeLayout) view.findViewById(R.id.searchLayout);
        searchEditText = (EditText) view.findViewById(R.id.searchEditText);

        setToolbar(view);
        setEditTextListeners();
        setRecyclerView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this)
                .get(MainViewModel.class);

        if (ingredients == null) {
            placeholder.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
        } else {
            mViewModel.getRecipes(ingredients, 0, 5).observe(this, recipes -> {
                placeholder.setVisibility(View.INVISIBLE);
                mRecyclerView.setVisibility(View.VISIBLE);
                adapter.setDataset(recipes);
            });
        }
    }

    //TODO: lekezelni, ha nincs találat!
    //TODO: from, to -t kitalálni
    private void searchRecipes(String inputIngredients) {
        if (inputIngredients == null) {
            Utils.showToast(getActivity(), getResources().getString(R.string.plsWriteSomething), Toast.LENGTH_SHORT);
            return;
        }

        ingredients = inputIngredients;
        mViewModel.getNewRecipes(ingredients, 0, 5).observe(this, recipes -> {
            placeholder.setVisibility(View.INVISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            adapter.setDataset(recipes);
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        searchItem = menu.findItem(R.id.searchItem);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.searchItem:
                showMenuItem(searchItem, false);
                return true;
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setEditTextListeners() {
        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard(v);
                showMenuItem(searchItem, true);
                searchRecipes(searchEditText.getText().toString());
                searchEditText.getText().clear();
                handled = true;
            }
            return handled;
        });

        searchEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                hideKeyboard(v);
            } else {
                showKeyboard(searchEditText);
            }
        });
    }

    private void showMenuItem(MenuItem item, boolean isMenuItemVisible) {
        item.setVisible(isMenuItemVisible);
        if (isMenuItemVisible) {
            searchLayout.setVisibility(View.INVISIBLE);
        } else {
            searchLayout.setVisibility(View.VISIBLE);
            searchEditText.requestFocus();
        }
    }

    private void showKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setRecyclerView(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mainRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new MainAdapter(getActivity(), mDataset);
        mRecyclerView.setAdapter(adapter);
    }

    private void setToolbar(View view) {
        setHasOptionsMenu(true);
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        ActionBar actionbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.logo);

        setupNavigationView(view);
    }

    private void setupNavigationView(View view) {
        NavigationView navigationView = view.findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.navFavorites:
                            mDrawerLayout.closeDrawers();
                            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_favoritesFragment);
                            return true;
                        case R.id.navShoppingCart:
                            mDrawerLayout.closeDrawers();
                            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_shoppingCartFragment);
                            return true;
                        case R.id.logout:
                            mDrawerLayout.closeDrawers();
                            SharedPrefUtils.clearSharedPreferences(getActivity());
                            startActivity(new Intent(getActivity(), AuthenticationActivity.class));
                            return true;
                        default:
                            return false;
                    }
                });
    }
}