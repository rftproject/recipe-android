package hu.unideb.rft.app;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import hu.unideb.rft.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
