package hu.unideb.rft.asynctask;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.AuthenticationCall;
import hu.unideb.rft.apicall.model.LoginRequest;
import hu.unideb.rft.apicall.model.LoginResponse;
import hu.unideb.rft.app.App;
import hu.unideb.rft.utils.SharedPrefKey;
import hu.unideb.rft.utils.SharedPrefUtils;

public class LoginTask extends AsyncTask<Void, Void, Boolean> {
    private String userName;
    private String password;

    public LoginTask() {
    }

    public LoginTask(String userName, String password) {
        this();
        this.userName = userName;
        this.password = password;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        if (userName != null && password != null) {
            return loginWithUsernameAndPassword();
        } else {
            return false;
        }
    }

    private boolean loginWithUsernameAndPassword() {
        final LoginResponse response = mobileLogin();
        if (response == null || response.getToken() == null) {
            return false;
        }

        saveAccount(response);
        return true;
    }

    private void saveAccount(LoginResponse response) {
        SharedPreferences.Editor editor = SharedPrefUtils.getInstance(App.getAppContext()).edit();
        editor.putLong(SharedPrefKey.USER_ID.getKey(), response.getUsername());
        editor.putString(SharedPrefKey.LOGIN_TOKEN.getKey(), response.getToken());
        editor.apply();
    }

    @Nullable
    private LoginResponse mobileLogin() {
        AuthenticationCall call = new AuthenticationCall();
        return call.mobileLogin(new LoginRequest(userName, password));
    }
}
