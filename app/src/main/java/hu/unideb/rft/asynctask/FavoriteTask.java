package hu.unideb.rft.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import hu.unideb.rft.apicall.FavoriteCall;
import hu.unideb.rft.apicall.model.Favorite;

public class FavoriteTask  {
    private final String TAG = this.getClass().getSimpleName();
    private FavoriteCall call;

    public FavoriteTask() {
        this.call = new FavoriteCall();
    }

    public Void addFavorite(Favorite favorite) {
        try {
            return new AddFavoriteAsyncTask(call, favorite).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "AddFavoriteAsyncTask failure", e);
            return null;
        }
    }

    public Void deleteFavoriteByUriAndUserId(Favorite favorite) {
        try {
            return new DeleteFavoriteAsyncTask(call, favorite).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "DeleteFavoriteAsyncTask failure", e);
            return null;
        }
    }

    public List<Favorite> getFavoritesByUserId(Long userId) {
        try {
            return new FavoritesByUserIdAsyncTask(call, userId).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "FavoritesByUserIdAsyncTask failure", e);
            return null;
        }
    }

    public Boolean isFavorite(Favorite favorite) {
        try {
            return new IsFavoriteAsyncTask(call, favorite).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "IsFavoriteAsyncTask failure", e);
            return null;
        }
    }

    private static class AddFavoriteAsyncTask extends AsyncTask<Void, Void, Void> {
        private FavoriteCall call;
        private Favorite favorite;

        AddFavoriteAsyncTask(FavoriteCall call, Favorite favorite) {
            this.call = call;
            this.favorite = favorite;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            return call.addFavorite(favorite);
        }
    }

    private static class DeleteFavoriteAsyncTask extends AsyncTask<Void, Void, Void> {
        private FavoriteCall call;
        private Favorite favorite;

        DeleteFavoriteAsyncTask(FavoriteCall call, Favorite favorite) {
            this.call = call;
            this.favorite = favorite;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            return call.deleteFavoriteByUriAndUserId(favorite);
        }
    }

    private static class FavoritesByUserIdAsyncTask extends AsyncTask<Void, Void, List<Favorite>> {
        private FavoriteCall call;
        private Long userId;

        FavoritesByUserIdAsyncTask(FavoriteCall call, Long userId) {
            this.call = call;
            this.userId = userId;
        }

        @Override
        protected List<Favorite> doInBackground(final Void... params) {
            return call.getFavoritesByUserId(userId);
        }
    }

    private static class IsFavoriteAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private FavoriteCall call;
        private Favorite favorite;

        IsFavoriteAsyncTask(FavoriteCall call, Favorite favorite) {
            this.call = call;
            this.favorite = favorite;
        }

        @Override
        protected Boolean doInBackground(final Void... params) {
            return call.isFavorite(favorite);
        }
    }
}
