package hu.unideb.rft.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import hu.unideb.rft.apicall.CartCall;
import hu.unideb.rft.apicall.model.Cart;

public class CartTask {
    private final String TAG = this.getClass().getSimpleName();
    private CartCall call;

    public CartTask() {
        this.call = new CartCall();
    }

    public Void addCart(Cart cart) {
        try {
            return new AddCartAsyncTask(call, cart).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "AddCartAsyncTask failure", e);
            return null;
        }
    }

    public Void deleteCartByUriAndUserId(Cart cart) {
        try {
            return new DeleteCartByUriAndUserIdAsyncTask(call, cart).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "DeleteCartByUriAndUserIdAsyncTask failure", e);
            return null;
        }
    }

    public List<Cart> getCartsByUserId(Long userId) {
        try {
            return new CartsByUserIdAsyncTask(call, userId).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "CartsByUserIdAsyncTask failure", e);
            return null;
        }
    }

    public Boolean isInCart(Cart cart) {
        try {
            return new IsInCartAsyncTask(call, cart).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "IsInCartAsyncTask failure", e);
            return null;
        }
    }

    private static class AddCartAsyncTask extends AsyncTask<Void, Void, Void> {
        private CartCall call;
        private Cart favorite;

        AddCartAsyncTask(CartCall call, Cart favorite) {
            this.call = call;
            this.favorite = favorite;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            return call.addCart(favorite);
        }
    }

    private static class DeleteCartByUriAndUserIdAsyncTask extends AsyncTask<Void, Void, Void> {
        private CartCall call;
        private Cart cart;

        DeleteCartByUriAndUserIdAsyncTask(CartCall call, Cart cart) {
            this.call = call;
            this.cart = cart;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            return call.deleteCartByUriAndUserId(cart);
        }
    }

    private static class CartsByUserIdAsyncTask extends AsyncTask<Void, Void, List<Cart>> {
        private CartCall call;
        private Long userId;

        CartsByUserIdAsyncTask(CartCall call, Long userId) {
            this.call = call;
            this.userId = userId;
        }

        @Override
        protected List<Cart> doInBackground(final Void... params) {
            return call.getCartsByUserId(userId);
        }
    }

    private static class IsInCartAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private CartCall call;
        private Cart cart;

        IsInCartAsyncTask(CartCall call, Cart cart) {
            this.call = call;
            this.cart = cart;
        }

        @Override
        protected Boolean doInBackground(final Void... params) {
            return call.isInCart(cart);
        }
    }
}
