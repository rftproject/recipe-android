package hu.unideb.rft.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.ExecutionException;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.AuthenticationCall;
import hu.unideb.rft.apicall.model.RegistrationRequest;
import hu.unideb.rft.apicall.model.RegistrationResponse;

public class RegistrationTask {
    private final String TAG = this.getClass().getSimpleName();
    private AuthenticationCall call;

    public RegistrationTask() {
        this.call = new AuthenticationCall();
    }

    @Nullable
    public RegistrationResponse registration(RegistrationRequest request) {
        try {
            return new RegistrationAsyncTask(call, request).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "registration failure", e);
            return null;
        }
    }

    private static class RegistrationAsyncTask extends AsyncTask<Void, Void, RegistrationResponse> {
        private AuthenticationCall call;
        private RegistrationRequest request;

        RegistrationAsyncTask(AuthenticationCall call, RegistrationRequest request) {
            this.call = call;
            this.request = request;
        }

        @Override
        protected RegistrationResponse doInBackground(final Void... params) {
            return call.registration(request);
        }
    }
}
