package hu.unideb.rft.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.RecipeCall;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.apicall.model.RecipeByIdRequest;

public class RecipeTask {
    private final String TAG = this.getClass().getSimpleName();
    private RecipeCall call;

    public RecipeTask() {
        this.call = new RecipeCall();
    }

    @Nullable
    public List<Recipe> getRecipes(String ingredients, int from, int to) {
        try {
            return new DownloadRecipesAsyncTask(call, ingredients, from, to).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "DownloadRecipesAsyncTask failure", e);
            return null;
        }
    }

    @Nullable
    public Recipe getRecipeById(String id) {
        try {
            return new RecipeByIdAsyncTask(call, id).execute().get();
        } catch (Exception e) {
            Log.e(TAG, "getRecipeById failure", e);
            return null;
        }
    }

    private static class DownloadRecipesAsyncTask extends AsyncTask<Void, Void, List<Recipe>> {
        private RecipeCall call;
        private String ingredients;
        private int from;
        private int to;

        DownloadRecipesAsyncTask(RecipeCall call, String ingredients, int from, int to) {
            this.call = call;
            this.ingredients = ingredients;
            this.from = from;
            this.to = to;
        }

        @Override
        protected List<Recipe> doInBackground(final Void... params) {
            return call.getRecipes(ingredients, from, to);
        }
    }

    private static class RecipeByIdAsyncTask extends AsyncTask<Void, Void, Recipe> {
        private RecipeCall call;
        private String id;

        RecipeByIdAsyncTask(RecipeCall call, String id) {
            this.call = call;
            this.id = id;
        }

        @Override
        protected Recipe doInBackground(final Void... params) {
            return call.getRecipeById(new RecipeByIdRequest(id));
        }
    }
}
