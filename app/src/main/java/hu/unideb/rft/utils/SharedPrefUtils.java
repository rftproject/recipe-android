package hu.unideb.rft.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public final class SharedPrefUtils {
    private static SharedPreferences pref;

    public static SharedPreferences getInstance(Context context) {
        if (pref == null) {
            pref = context.getApplicationContext().getSharedPreferences(SharedPrefKey.PREF_NAME.getKey(), MODE_PRIVATE);
        }
        return pref;
    }

    public static Long getLoginedUserId(Context context) {
        return getInstance(context).getLong(SharedPrefKey.USER_ID.getKey(), 0);
    }

    public static String getLoginToken(Context context) {
        return getInstance(context).getString(SharedPrefKey.LOGIN_TOKEN.getKey(), null);
    }

    public static void clearSharedPreferences(Context context) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.clear();
        editor.apply();
    }
}
