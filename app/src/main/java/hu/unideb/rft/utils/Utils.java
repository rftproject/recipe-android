package hu.unideb.rft.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

public final class Utils {
    private static Toast toast;

    public static void showToast(Context context, String message, int toastDuration) {
        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            toast = Toast.makeText(context, message, toastDuration);
            toast.show();
        }
    }

    public static void showKeyboard(EditText editText, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void enableTouch(ConstraintLayout parentLayout, boolean isEnabled) {
        if (isEnabled) {
            parentLayout.setAlpha(1.0f);
        } else {
            parentLayout.setAlpha(0.3f);
        }
        parentLayout.setEnabled(isEnabled);
    }
}
