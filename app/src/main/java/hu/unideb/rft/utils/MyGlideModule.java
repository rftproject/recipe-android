package hu.unideb.rft.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import hu.unideb.rft.R;

@GlideModule
public class MyGlideModule extends AppGlideModule {

    public static void loadImage(Context context, String imgUrl, ImageView mainImage) {
        GlideApp.with(context)
                .load(imgUrl)
                .placeholder(R.drawable.placeholder)
                .fallback(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(mainImage);
    }
}