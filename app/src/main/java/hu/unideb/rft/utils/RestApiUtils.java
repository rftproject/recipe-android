package hu.unideb.rft.utils;

import java.util.List;

import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.apicall.model.Favorite;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.asynctask.CartTask;
import hu.unideb.rft.asynctask.FavoriteTask;
import hu.unideb.rft.asynctask.RecipeTask;

public class RestApiUtils {

    public static Recipe getRecipeById(String uri) {
        return new RecipeTask().getRecipeById(uri);
    }

    public static void addFavorite(Favorite favorite) {
         new FavoriteTask().addFavorite(favorite);
    }

    public static void deleteFavoriteByUriAndUserId(Favorite favorite) {
        new FavoriteTask().deleteFavoriteByUriAndUserId(favorite);
    }

    public static List<Favorite> getFavoritesByUserId(Long userId) {
        return new FavoriteTask().getFavoritesByUserId(userId);
    }

    public static Boolean isFavorite(Favorite favorite) {
        return new FavoriteTask().isFavorite(favorite);
    }

    public static void addCart(Cart cart) {
        new CartTask().addCart(cart);
    }

    public static List<Cart> getCartsByUserId(Long userId) {
        return new CartTask().getCartsByUserId(userId);
    }

    public static void deleteCartByUriAndUserId(Cart cart) {
        new CartTask().deleteCartByUriAndUserId(cart);
    }

    public static Boolean isInCart(Cart cart) {
        return new CartTask().isInCart(cart);
    }
}
