package hu.unideb.rft.utils;

public enum ValidationMessages {
    NAME_EMPTY_FIELD("reg_name_required"),
    EMAIL_EMPTY_FIELD("reg_email_required"),
    PASSWORD_EMPTY_FIELD("reg_password_required"),
    PASSWORD_NOT_MATCHING("reg_pw_not_matching"),
    WEAK_PASSWORD("reg_weak_pw"),
    PASSWORD_REQUIREMENTS("password_requirements");

    private final String message;

    ValidationMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
