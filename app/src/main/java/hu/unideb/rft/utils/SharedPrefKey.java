package hu.unideb.rft.utils;

public enum SharedPrefKey {
    PREF_NAME("recipePref"),
    USER_ID("userId"),
    LOGIN_TOKEN("loginToken");

    private final String key;

    SharedPrefKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}

