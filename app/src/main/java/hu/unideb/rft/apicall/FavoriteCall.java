package hu.unideb.rft.apicall;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.model.Favorite;
import retrofit2.Call;
import retrofit2.Response;

public class FavoriteCall {
    private final String TAG = this.getClass().getSimpleName();
    private RecipeApi api;

    public FavoriteCall() {
        api = ApiClient.getClient()
                .create(RecipeApi.class);
    }

    @Nullable
    public Void addFavorite(Favorite favorite) {
        Call<Void> call = api.addFavorite(favorite);

        try {
            final Response<Void> response = call.execute();

            if (!response.isSuccessful() || response.body() == null) {
                call.cancel();
                Log.e(TAG, "addFavorite() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "addFavorite() failed", e);
        }

        return null;
    }

    @Nullable
    public Void deleteFavoriteByUriAndUserId(Favorite favorite) {
        Call<Void> call = api.deleteFavoriteByUriAndUserId(favorite);

        try {
            final Response<Void> response = call.execute();

            if (!response.isSuccessful() || response.body() == null) {
                call.cancel();
                Log.e(TAG, "deleteFavoriteByUriAndUserId() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "deleteFavoriteByUriAndUserId() failed", e);
        }

        return null;
    }

    @Nullable
    public List<Favorite> getFavoritesByUserId(Long userId) {
        Call<List<Favorite>> call = api.getFavoritesByUserId(userId);

        try {
            final Response<List<Favorite>> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "getFavoritesByUserId() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "getFavoritesByUserId() failed", e);
        }

        return null;
    }

    @Nullable
    public Boolean isFavorite(Favorite favorite) {
        Call<Boolean> call = api.isFavorite(favorite);

        try {
            final Response<Boolean> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "isFavorite() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "isFavorite() failed", e);
        }

        return null;
    }
}
