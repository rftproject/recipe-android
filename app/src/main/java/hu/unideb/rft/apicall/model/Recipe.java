package hu.unideb.rft.apicall.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Recipe {
    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("ingredientLines")
    @Expose
    private List<String> ingredientLines;

    @SerializedName("calories")
    @Expose
    private Double calories;

    @SerializedName("yield")
    @Expose
    private Double yield;

    @SerializedName("totalTime")
    @Expose
    private Double totalTime;

    @SerializedName("uri")
    @Expose
    private String uri;

    private String source;

    private String url;

    private String shareAs;

    private List<String> dietLabels;

    private List<String> healthLabels;

    private List<Object> cautions;

    private List<Object> ingredients;

    private Double totalWeight;

    private Object totalNutrients;

    private Object totalDaily;

    private List<Object> digest;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Object> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Object> ingredients) {
        this.ingredients = ingredients;
    }

    public Double getCalories() {
        return calories;
    }

    public void setCalories(Double calories) {
        this.calories = calories;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShareAs() {
        return shareAs;
    }

    public void setShareAs(String shareAs) {
        this.shareAs = shareAs;
    }

    public Double getYield() {
        return yield;
    }

    public void setYield(Double yield) {
        this.yield = yield;
    }

    public List<String> getDietLabels() {
        return dietLabels;
    }

    public void setDietLabels(List<String> dietLabels) {
        this.dietLabels = dietLabels;
    }

    public List<String> getHealthLabels() {
        return healthLabels;
    }

    public void setHealthLabels(List<String> healthLabels) {
        this.healthLabels = healthLabels;
    }

    public List<Object> getCautions() {
        return cautions;
    }

    public void setCautions(List<Object> cautions) {
        this.cautions = cautions;
    }

    public ArrayList<String> getIngredientLines() {
        return new ArrayList<>(ingredientLines);
    }

    public void setIngredientLines(List<String> ingredientLines) {
        this.ingredientLines = ingredientLines;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Double totalTime) {
        this.totalTime = totalTime;
    }

    public Object getTotalNutrients() {
        return totalNutrients;
    }

    public void setTotalNutrients(Object totalNutrients) {
        this.totalNutrients = totalNutrients;
    }

    public Object getTotalDaily() {
        return totalDaily;
    }

    public void setTotalDaily(Object totalDaily) {
        this.totalDaily = totalDaily;
    }

    public List<Object> getDigest() {
        return digest;
    }

    public void setDigest(List<Object> digest) {
        this.digest = digest;
    }
}
