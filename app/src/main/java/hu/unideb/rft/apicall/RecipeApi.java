package hu.unideb.rft.apicall;

import java.util.List;

import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.apicall.model.Favorite;
import hu.unideb.rft.apicall.model.LoginRequest;
import hu.unideb.rft.apicall.model.LoginResponse;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.apicall.model.RecipeByIdRequest;
import hu.unideb.rft.apicall.model.RegistrationRequest;
import hu.unideb.rft.apicall.model.RegistrationResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RecipeApi {
    @POST("token/login")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("/token/register")
    Call<RegistrationResponse> registration(@Body RegistrationRequest request);

    @GET("api/recipes/{ingredients}/{from}/{to}")
    Call<List<Recipe>> getRepicesFromIngredients(@Path("ingredients") String ingredients,
                                                 @Path("from") int from,
                                                 @Path("to") int to);

    @POST("/api/recipes/id")
    Call<Recipe> getRecipeById(@Body RecipeByIdRequest request);

    @POST("api/favorites/add")
    Call<Void> addFavorite(@Body Favorite favorite);

    @POST("api/favorites/delete-by-uri-and-userid")
    Call<Void> deleteFavoriteByUriAndUserId(@Body Favorite favorite);

    @GET("api/favorites/{userId}")
    Call<List<Favorite>> getFavoritesByUserId(@Path("userId") Long userId);

    @POST("api/favorites/isfavorite")
    Call<Boolean> isFavorite(@Body Favorite favorite);

    @POST("api/cart/add")
    Call<Void> addCart(@Body Cart cart);

    @POST("api/cart/delete-by-uri-and-userid")
    Call<Void> deleteCartByUriAndUserId(@Body Cart cart);

    @GET("api/cart/{userId}")
    Call<List<Cart>> getCartsByUserId(@Path("userId") Long userId);

    @POST("api/cart/isincart")
    Call<Boolean> isInCart(@Body Cart cart);
}
