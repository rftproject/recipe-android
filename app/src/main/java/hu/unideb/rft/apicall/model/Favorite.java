package hu.unideb.rft.apicall.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Favorite {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("uri")
    @Expose
    private String uri;

    @SerializedName("userId")
    @Expose
    private Long userId;

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("ingredients")
    @Expose
    private List<String> ingredients;

    @SerializedName("image")
    @Expose
    private String image;

     public Favorite(String uri, Long userId) {
        this.uri = uri;
        this.userId = userId;
    }

    public Favorite(String uri, Long userId, String label, List<String> ingredients, String image) {
        this.uri = uri;
        this.userId = userId;
        this.label = label;
        this.ingredients = ingredients;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}


