package hu.unideb.rft.apicall.model;

public class LoginResponse {
    private Long username;
    private String token;

    public LoginResponse(Long username, String token) {
        this.username = username;
        this.token = token;
    }

    public Long getUsername() {
        return username;
    }

    public void setUsername(Long username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
