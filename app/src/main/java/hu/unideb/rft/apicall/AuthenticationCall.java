package hu.unideb.rft.apicall;

import android.util.Log;

import java.io.IOException;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.model.LoginRequest;
import hu.unideb.rft.apicall.model.LoginResponse;
import hu.unideb.rft.apicall.model.RegistrationRequest;
import hu.unideb.rft.apicall.model.RegistrationResponse;
import retrofit2.Call;
import retrofit2.Response;

public class AuthenticationCall {
    private final String TAG = this.getClass().getSimpleName();
    private RecipeApi api;

    public AuthenticationCall() {
        api = ApiClient.getClientWithoutInterceptor()
                .create(RecipeApi.class);
    }

    @Nullable
    public LoginResponse mobileLogin(LoginRequest request) {
        Call<LoginResponse> call = api.login(request);

        try {
            final Response<LoginResponse> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "mobileLogin() DOES NOT success!!" + response.errorBody());
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "mobileLogin() failed", e);
            return null;
        }
    }

    @Nullable
    public RegistrationResponse registration(RegistrationRequest request) {
        Call<RegistrationResponse> call = api.registration(request);

        try {
            final Response<RegistrationResponse> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "register() DOES NOT success!!" + response.errorBody());
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "register() failed", e);
            return null;
        }
    }
}
