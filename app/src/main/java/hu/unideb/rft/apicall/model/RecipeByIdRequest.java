package hu.unideb.rft.apicall.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecipeByIdRequest {
    @SerializedName("id")
    @Expose
    private String id;

    public RecipeByIdRequest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
