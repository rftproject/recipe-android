package hu.unideb.rft.apicall;

import com.google.gson.GsonBuilder;

import hu.unideb.rft.R;
import hu.unideb.rft.app.App;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit;
    private static Retrofit retrofitWithoutInterceptor;
    private final static String BASE_URL = App.getAppContext().getResources().getString(R.string.baseURL);

    public static Retrofit getClient() {
        if (retrofit != null) {
            return retrofit;
        }

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new MyOkHttpInterceptor(App.getAppContext()))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                .client(client)
                .build();

        return retrofit;
    }

    public static Retrofit getClientWithoutInterceptor() {
        if (retrofitWithoutInterceptor != null) {
            return retrofitWithoutInterceptor;
        }

        retrofitWithoutInterceptor = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();

        return retrofitWithoutInterceptor;
    }

    public String getBaseUrl() {
        return BASE_URL;
    }
}
