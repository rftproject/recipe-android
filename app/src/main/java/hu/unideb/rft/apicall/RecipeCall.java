package hu.unideb.rft.apicall;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.apicall.model.RecipeByIdRequest;
import retrofit2.Call;
import retrofit2.Response;

public class RecipeCall {
    private final String TAG = this.getClass().getSimpleName();
    private RecipeApi api;

    public RecipeCall() {
        api = ApiClient.getClient()
                .create(RecipeApi.class);
    }

    public List<Recipe> getRecipes(String ingredients, int from, int to) {
        Call<List<Recipe>> call = api.getRepicesFromIngredients(ingredients, from, to);

        try {
            Response<List<Recipe>> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                Log.e(TAG, "getRecipes DOES NOT success!!" + response.errorBody());
                call.cancel();
            }
        }catch (IOException e) {
            Log.e(TAG, "getRecipes failure",e);
            call.cancel();
        }

        return null;
    }

    public Recipe getRecipeById(RecipeByIdRequest request) {
        Call<Recipe> call = api.getRecipeById(request);

        try {
            Response<Recipe> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                Log.e(TAG, "getRecipeById DOES NOT success!!" + response.errorBody());
                call.cancel();
            }
        }catch (IOException e) {
            Log.e(TAG, "getRecipeById failure",e);
            call.cancel();
        }

        return null;
    }
}
