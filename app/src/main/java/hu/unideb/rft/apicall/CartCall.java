package hu.unideb.rft.apicall;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import androidx.annotation.Nullable;
import hu.unideb.rft.apicall.model.Cart;
import retrofit2.Call;
import retrofit2.Response;

public class CartCall {
    private final String TAG = this.getClass().getSimpleName();
    private RecipeApi api;

    public CartCall() {
        api = ApiClient.getClient()
                .create(RecipeApi.class);
    }

    @Nullable
    public Void addCart(Cart cart) {
        Call<Void> call = api.addCart(cart);

        try {
            final Response<Void> response = call.execute();

            if (!response.isSuccessful() || response.body() == null) {
                call.cancel();
                Log.e(TAG, "addCart() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "addCart() failed", e);
        }

        return null;
    }

    @Nullable
    public Void deleteCartByUriAndUserId(Cart cart) {
        Call<Void> call = api.deleteCartByUriAndUserId(cart);

        try {
            final Response<Void> response = call.execute();

            if (!response.isSuccessful() || response.body() == null) {
                call.cancel();
                Log.e(TAG, "deleteCartById() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "deleteCartById() failed", e);
        }

        return null;
    }

    @Nullable
    public List<Cart> getCartsByUserId(Long userId) {
        Call<List<Cart>> call = api.getCartsByUserId(userId);

        try {
            final Response<List<Cart>> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "getCartsByUserId() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "getCartsByUserId() failed", e);
        }

        return null;
    }

    @Nullable
    public Boolean isInCart(Cart cart) {
        Call<Boolean> call = api.isInCart(cart);

        try {
            final Response<Boolean> response = call.execute();

            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            } else {
                call.cancel();
                Log.e(TAG, "isInCart() DOES NOT success!!" + response.errorBody());
            }
        } catch (IOException e) {
            Log.e(TAG, "isInCart() failed", e);
        }

        return null;
    }
}
