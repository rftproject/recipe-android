package hu.unideb.rft.apicall;

import android.content.Context;

import java.io.IOException;

import androidx.annotation.NonNull;
import hu.unideb.rft.utils.SharedPrefUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class MyOkHttpInterceptor implements Interceptor {
    private String authToken;

    MyOkHttpInterceptor(Context context) {
        this.authToken = SharedPrefUtils.getLoginToken(context);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer " + authToken)
                .build();

        return chain.proceed(newRequest);
    }
}
