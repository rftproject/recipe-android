package hu.unideb.rft.apicall.model;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import hu.unideb.rft.utils.ValidationMessages;

import static hu.unideb.rft.utils.ValidationMessages.EMAIL_EMPTY_FIELD;
import static hu.unideb.rft.utils.ValidationMessages.NAME_EMPTY_FIELD;
import static hu.unideb.rft.utils.ValidationMessages.PASSWORD_EMPTY_FIELD;
import static hu.unideb.rft.utils.ValidationMessages.PASSWORD_NOT_MATCHING;
import static hu.unideb.rft.utils.ValidationMessages.PASSWORD_REQUIREMENTS;
import static hu.unideb.rft.utils.ValidationMessages.WEAK_PASSWORD;

public class RegistrationRequest {
    private String username;
    private String password;
    private String password2;
    private String email;
    private String name;

    public RegistrationRequest(String username, String password, String password2, String email, String name) {
        this.username = username;
        this.password = password;
        this.password2 = password2;
        this.email = email;
        this.name = name;
    }

    public List<ValidationMessages> validate() {
        List<ValidationMessages> messages = new ArrayList<>();

        if (TextUtils.isEmpty(name)) {
            messages.add(NAME_EMPTY_FIELD);
        }
        if (TextUtils.isEmpty(username)) {
            messages.add(EMAIL_EMPTY_FIELD);
        }
        if (TextUtils.isEmpty(password) || TextUtils.isEmpty(password2)) {
            messages.add(PASSWORD_EMPTY_FIELD);
        } else {
            if (!password.equals(password2)) {
                messages.add(PASSWORD_NOT_MATCHING);
            }
            if (password.length() < 4) {
                messages.add(WEAK_PASSWORD);
                messages.add(PASSWORD_REQUIREMENTS);
            }
        }
        return messages;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
