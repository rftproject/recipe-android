package hu.unideb.rft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hu.unideb.rft.R;
import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.utils.MyGlideModule;

public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingViewHolder> {
    private Context context;
    private List<Cart> carts;

    public ShoppingAdapter(Context context, List<Cart> carts) {
        this.context = context;
        this.carts = carts;
    }

    @NonNull
    @Override
    public ShoppingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_item, parent, false);
        itemView.setFocusable(true);
        return new ShoppingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingViewHolder holder, int position) {
        holder.labelTv.setText(carts.get(position).getLabel());
        MyGlideModule.loadImage(context, carts.get(position).getImage(), holder.imageView);
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return carts == null ? 0 : carts.size();
    }
}
