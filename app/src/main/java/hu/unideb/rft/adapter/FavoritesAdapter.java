package hu.unideb.rft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.unideb.rft.R;
import hu.unideb.rft.apicall.model.Favorite;
import hu.unideb.rft.utils.MyGlideModule;

public class FavoritesAdapter extends BaseAdapter {
    private final Context context;
    private final List<Favorite> favorites;

    public FavoritesAdapter(Context context, List<Favorite> favorites) {
        this.context = context;
        this.favorites = favorites;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.favorite_list_item, parent, false);
        }

        final Favorite recipe = favorites.get(position);
        final ImageView img = (ImageView) view.findViewById(R.id.gridImg);
        final TextView labelTV = (TextView) view.findViewById(R.id.gridLabel);

        MyGlideModule.loadImage(context, recipe.getImage(), img);
        labelTV.setText(recipe.getLabel());

        return view;
    }

    @Override
    public int getCount() {
        return favorites == null ? 0 : favorites.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }
}