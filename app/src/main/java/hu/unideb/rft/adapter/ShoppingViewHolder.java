package hu.unideb.rft.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import hu.unideb.rft.R;

public class ShoppingViewHolder extends RecyclerView.ViewHolder {
    protected CircleImageView imageView;
    protected TextView labelTv;

    ShoppingViewHolder(@NonNull View view) {
        super(view);
        imageView = (CircleImageView) view.findViewById(R.id.cartImage);
        labelTv = (TextView) view.findViewById(R.id.cartLabel);
    }
}
