package hu.unideb.rft.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import hu.unideb.rft.R;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.utils.MyGlideModule;

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private Context context;
    private List<Recipe> mDataset;

    public MainAdapter(Context context, List<Recipe> mDataset) {
        this.context = context;
        this.mDataset = mDataset;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_list_item, parent, false);
        itemView.setFocusable(true);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        holder.label.setText(mDataset.get(position).getLabel());
        holder.numOfCalories.setText(String.valueOf(mDataset.get(position).getCalories().intValue()));
        holder.numOfIngredients.setText(String.valueOf(mDataset.get(position).getIngredientLines().size()));
        MyGlideModule.loadImage(context, mDataset.get(position).getImage(), holder.mainImage);
        setImageViewOnClickListener(holder.mainImage, mDataset.get(position).getUri());
    }

    public void setDataset(List<Recipe> recipes) {
        mDataset = recipes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    private void setImageViewOnClickListener(ImageView imageView, String uri) {
        imageView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("uri", uri);
            Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_detailsFragment, bundle);
        });
    }
}
