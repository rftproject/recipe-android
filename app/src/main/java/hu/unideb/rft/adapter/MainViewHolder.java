package hu.unideb.rft.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import hu.unideb.rft.R;

public class MainViewHolder extends RecyclerView.ViewHolder {
    protected TextView label;
    protected TextView numOfCalories;
    protected TextView numOfIngredients;
    protected ImageView mainImage;

    MainViewHolder(@NonNull View view) {
        super(view);
        label = (TextView) view.findViewById(R.id.label);
        numOfCalories = (TextView) view.findViewById(R.id.numOfCalories);
        numOfIngredients = (TextView) view.findViewById(R.id.numOfIngredients);
        mainImage = (ImageView) view.findViewById(R.id.mainImage);
    }
}
