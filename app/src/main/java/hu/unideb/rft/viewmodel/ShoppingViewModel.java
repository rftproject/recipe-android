package hu.unideb.rft.viewmodel;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import hu.unideb.rft.apicall.model.Cart;
import hu.unideb.rft.utils.RestApiUtils;

public class ShoppingViewModel extends AndroidViewModel {
    private MutableLiveData<List<Cart>> carts;

    public ShoppingViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<List<Cart>> getShoppingCarts(Long userId) {
        if (carts == null) {
            carts = new MutableLiveData<>();
        }
        carts.setValue(RestApiUtils.getCartsByUserId(userId));
        return carts;
    }
}
