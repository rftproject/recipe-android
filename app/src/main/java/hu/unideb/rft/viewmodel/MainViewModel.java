package hu.unideb.rft.viewmodel;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import hu.unideb.rft.apicall.model.Recipe;
import hu.unideb.rft.asynctask.RecipeTask;

public class MainViewModel extends AndroidViewModel {
    private MutableLiveData<List<Recipe>> recipes;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<List<Recipe>> getRecipes(String ingredients, int from, int to) {
        if (recipes == null) {
            recipes = new MutableLiveData<>();
            downloadRecipes(ingredients, from, to);
        }

        return recipes;
    }

    public MutableLiveData<List<Recipe>> getNewRecipes(String ingredients, int from, int to) {
        if (recipes == null) {
            recipes = new MutableLiveData<>();
        }

        downloadRecipes(ingredients, from, to);
        return recipes;
    }

    private void downloadRecipes(String ingredients, int from, int to) {
        final List<Recipe> recipeList = new RecipeTask().getRecipes(ingredients, from, to);
        this.recipes.setValue(recipeList);
    }
}
